#!/bin/bash 
# Update the Repository
sudo apt-get update 

# Install Utility Softwares
sudo apt-get install vim curl wget unzip elinks tree git -y 

# Download, Install & Configure WebServer i.e. Apache - apache2 
sudo apt-get install apache2 -y 

# Enable the Daemon / Service at Boot Level 
sudo systemctl enable apache2.service 

# Start the Daemon / Service 
sudo systemctl start apache2.service 

# Deploy a Code HTML, CSS & JS from github 
cd /opt/

git clone https://github.com/keshavkummari/keshavkummari.git
https://github.com/keshavkummari/codewithckk.git

cd keshavkummari/

# Move the Code to DocumentRoot /var/www/html 
mv * /var/www/html/

# Restart the Daemon / Service 
sudo systemctl restart apache2.service